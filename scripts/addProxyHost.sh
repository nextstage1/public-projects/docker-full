#!/bin/bash

clear

if [ $(id -u) -eq 0 ]; then
   echo "## Add new APP (Virtual Server on Apache Proxy and Path to App)"

   read -p "Enter url: " url
   read -p "Enter port redirect: " port

   dir="/dados/apps/$url"

   echo "- Directories create"
   mkdir $dir
   mkdir "$dir/build"
   chmod 0777 -R "$dir/build"
   chown ubuntu:ubuntu $dir -R

   #Apache
   echo "- Create site on apache"
   echo "
   <VirtualHost *:443>
      ServerAdmin ns@nextstage.com.br
      ProxyPreserveHost On
      ProxyPass / http://0.0.0.0:$port/
      ProxyPassReverse / http://0.0.0.0:$port/
      ServerName $url
   </VirtualHost>
   <VirtualHost *:80>
      ServerAdmin ns@nextstage.com.br
      ServerName $url
      Redirect / https://$url/
   </VirtualHost>" > "/etc/apache2/sites-available/$url.conf"

   echo "- Enable site"
   a2ensite "$url.conf"
   service apache2 restart

   echo "- Activate ssl"
   certbot --apache

   echo "Complete!"
else
	echo "## ERROR ## "
   echo "To continue, run this script with sudo"
   echo ""
	exit 2
fi


#!/bin/bash

if [ ! $(id -u) -eq 0 ]; then
    echo "## ERROR ## "
    echo "To continue, run this script with sudo"
    echo ""
    exit 2
fi

echo "\n   ### LetsEncrypt"

## navegar para dir da aplicacao
cd $(dirname $0);__DIR__=$(pwd)
cd ../
export $(grep -v '^#' .env | xargs -d '\n')
clear

#####################################
DOMAIN=domain.com
docker run -it --rm \
    --name certbot \
    -v "$(pwd)/${PERSISTPATH}/letsencrypt/certs":"/etc/letsencrypt" \
    -v "$(pwd)/${PERSISTPATH}/letsencrypt/data":"/data/letsencrypt" \
    certbot/certbot certonly --webroot --webroot-path=/data/letsencrypt --email "${LETSENCRYPT_EMAIL}" \
    -d ${DOMAIN} \
    -n \
    --agree-tos
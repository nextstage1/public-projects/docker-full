#!/bin/sh

## Usage:
##   . ./export-env.sh ; $COMMAND
##   . ./export-env.sh ; echo ${MINIENTREGA_FECHALIMITE}

if [ ! $(id -u) -eq 0 ]; then
   echo "## ERROR ## "
   echo "To continue, run this script with sudo"
   echo ""
   exit 2
fi

## navegar para dir da aplicacao
cd $(dirname $0); __DIR__=$(pwd)
cd ../

export $(grep -v '^#' .env | xargs -d '\n')

echo "Project Name: $COMPOSE_PROJECT_NAME"
#!/bin/bash

if [ ! $(id -u) -eq 0 ]; then
    echo "## ERROR ## "
    echo "To continue, run this script with sudo"
    echo ""
    exit 2
fi

## navegar para dir da aplicacao
cd $(dirname $0)
__DIR__=$(pwd)
cd ../
export $(grep -v '^#' .env | xargs -d '\n')
clear

# Atualização do composer das aplicações
APPPATH="/APPDIR/hml/www"
RELEASE="${PHP_APP_DIR}/${APPPATH}"
# docker exec -w $RELEASE ${COMPOSE_PROJECT_NAME}_php composer install --prefer-dist --optimize-autoloader --no-dev
# docker exec -w $RELEASE ${COMPOSE_PROJECT_NAME}_php composer update --prefer-dist --optimize-autoloader --no-dev
# docker exec -w "${APACHE_ROOT_DIR}/htdocs/${APPPATH}" ${COMPOSE_PROJECT_NAME}_apache npm install


# restartar para carregar alteracoes
# sh restart.sh

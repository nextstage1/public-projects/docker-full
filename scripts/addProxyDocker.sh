#!/bin/bash

clear

if [ $(id -u) -eq 0 ]; then
   echo "## Add new APP (Virtual Server on Apache Proxy and Path to App)"

   dir="${LOCAL_HTDOCS}/${url}"
   dirdocker="."

   read -p "Enter url: " url
   read -p "Enter port redirect: " port

   #Apache
   echo "- Create vhost with proxy"
   echo "
   <VirtualHost *:443>
      ServerName $url

      ServerAdmin ns@nextstage.com.br
      ProxyPreserveHost On
      ProxyPass / http://0.0.0.0:$port/
      ProxyPassReverse / http://0.0.0.0:$port/

      # antes do letsencrypt
      SSLCertificateKeyFile \${APACHE_ROOT_DIR}/certs/server.key
      SSLCertificateFile \${APACHE_ROOT_DIR}/certs/server.crt


      # Letsencrypt
      #SSLCertificateFile /home/letsencrypt/certs/live/${url}/cert.pem
      #SSLCertificateKeyFile /home/letsencrypt/certs/live/${url}/privkey.pem
      #SSLCertificateChainFile /home/letsencrypt/certs/live/${url}/fullchain.pem

      SSLProtocol ALL -SSLv2 -SSLv3
      SSLHonorCipherOrder On
      SSLCipherSuite ECDH+AESGCM:DH+AESGCM:ECDH+AES256:DH+AES256:ECDH+AES128:DH+AES:ECDH+3DES:DH+3DES:RSA+AESGCM:RSA+AES:RSA+3DES:!aNULL:!MD5
      SSLCompression Off

      Header always set Strict-Transport-Security \"max-age=31536000; includeSubDomains; preload\"
      Header always append X-Frame-Options sameorigin
   </VirtualHost>
   <VirtualHost *:80>
      ServerAdmin ns@nextstage.com.br
      ServerName $url
      Redirect / https://$url/
   </VirtualHost>" > ""${dirdocker}/apache/configs/vhosts/${url}-proxy.conf""

   echo "Complete! Restart docker.."
else
	echo "## ERROR ## "
   echo "To continue, run this script with sudo"
   echo ""
	exit 2
fi